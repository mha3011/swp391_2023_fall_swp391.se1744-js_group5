
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Nguyễn Khâm Hưng
 */
public class SearchCustomer extends DBContext{
    
    
    public void searchCustomer(String[] args) {
        // Thông tin kết nối cơ sở dữ liệu
        String jdbcURL = "jdbc:mysql://localhost:3306/SmartBeaty";
        String username = "admin";
        String password = "123";
        
        // ID bạn muốn truy vấn
        int idToQuery = 1;
        
        // Chuỗi truy vấn SQL
        String sqlQuery = "SELECT * FROM [dbo].[Customer] WHERE id = ?";
        
        try {
            // Kết nối đến cơ sở dữ liệu
            Connection connection = DriverManager.getConnection(jdbcURL, username, password);
            
            // Tạo PreparedStatement với ID là tham số
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, idToQuery);
            
            // Thực hiện truy vấn
            ResultSet resultSet = preparedStatement.executeQuery();
            
            // Xử lý kết quả truy vấn
            while (resultSet.next()) {
                // Lấy dữ liệu từ các cột của kết quả
                int id = resultSet.getInt("id");               
                String columnName1 = resultSet.getString("fullName");
                String columnName2 = resultSet.getString("Address");
                String columnName3 = resultSet.getString("phone");
                
                // Xử lý dữ liệu ở đây
                System.out.println("Id: " + id);
                System.out.println("Full Name: " + columnName1);
                System.out.println("Address: " + columnName2);
                System.out.println("Phone: " + columnName3);
            }
            
            // Đóng tất cả các kết nối
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
