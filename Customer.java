/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Nguyễn Khâm Hưng
 */
public class Customer {
    private String id;
    private String fullName;
    private String Address;
    private String phone;

    public Customer() {
    }

    public Customer(String id, String fullName, String Address, String phone) {
        this.id = id;
        this.fullName = fullName;
        this.Address = Address;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAddress() {
        return Address;
    }

    public String getPhone() {
        return phone;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", fullName=" + fullName + ", Address=" + Address + ", phone=" + phone + '}';
    }
    
    

    
    
    
    
    

}
