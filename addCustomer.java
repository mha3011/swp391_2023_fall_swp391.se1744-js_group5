
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Nguyễn Khâm Hưng
 */
public class addCustomer extends DBContext {

    public void addCustomer(String id, String fullName,String Address,String phone) {
        try {
            String sql = "INSERT INTO Customers"
                    + "  (id, fullName, Address, phone) VALUES "
                    + " (?, ?, ?, ?, ?);";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, fullName);
            ps.setString(3, Address);
            ps.setString(4, phone);

            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());

        }
    }

 
}
