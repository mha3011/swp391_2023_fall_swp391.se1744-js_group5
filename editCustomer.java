
import java.sql.PreparedStatement;
import java.sql.SQLException;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Nguyễn Khâm Hưng
 */
public class editCustomer extends DBContext {
       public void editProfile(String id, String fullName, String Address, String phone) {
        try {
            String sql = "EDIT [dbo].[Customer]\n"
                    + "SET [id] = ?,\n"
                    + "    [fullName] = ?,\n"
                    + "    [Address] = ?,\n"
                    + "    [phone] = ?,\n"
                    + "WHERE id = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, fullName);
            ps.setString(3, Address);
            ps.setString(4, phone);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e);
        }
    }
}
